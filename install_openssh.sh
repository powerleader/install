#!/bin/bash

# 更新软件包列表并安装编译依赖
sudo apt-get update
sudo apt-get install -y build-essential zlib1g-dev libssl-dev

# 下载指定版本源码
wget https://cdn.openbsd.org/pub/OpenBSD/OpenSSH/portable/openssh-9.8p1.tar.gz

# 解压并进入目录
tar -xzf openssh-9.8p1.tar.gz
cd openssh-9.8p1

# 编译和安装
./configure
make
sudo make install

# 重启服务
sudo systemctl restart ssh
